#include <iostream>
class Animal{
public:
    virtual  void Voice(){
        std::cout << "animal voice\n";
    }

    virtual ~Animal(){}
};

class Dog : public Animal{
    void Voice() override {
        std::cout << "Woof!\n";
    }
};

class Cat : public Animal{
    void Voice() override {
        std::cout << "Meow!\n";
    }
};

class Groot : public Animal{
    void Voice() override {
        std::cout << "I am Groot!\n";
    }
};

int main() {
    Animal* animals[4] = {new Dog, new Cat, new Groot, new Animal};
    for(int i = 0; i<4; i++){
        animals[i]->Voice();
        delete animals[i];
    }
    return 0;
}