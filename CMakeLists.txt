cmake_minimum_required(VERSION 3.15)
project(dz19)

set(CMAKE_CXX_STANDARD 14)

add_executable(dz19 main.cpp)